Il y a plusieurs branches à merger dans main, avec tous les cas entre merge auto, merge simple et 
merge qui force l'édition à la main.

Les branches sont:

- initial: la branche de départ, ne pas toucher
- initial_ignore: la branche dans laquelle j'ai ajouté un fichier .gitignore
- main: une copie de initial_ignore avec laquelle vous allez travailler
- pretty: initial avec une fonction pretty en plus et un peu de test de pretty
- comment_test: initial avec des commentaires et plus de test
- refac_no_comma: initial refactorée pour enlever les virgules qui pourraient troubler un débutant en python

Si vous executez test.py, ça doit toujours marcher. C'est donc le test pour vous assurer que les fusions se sont bien
passées.

Les fusions à tester sont :

- `git checkout main` puis `git merge pretty` : ça devrait être automatique, pas besoin de commit
- sur cette branche main modifiée, `git merge comment_test`: avec un outil graphique de fusion (celui que vous préférez)
  choisissez les modifs à appliquer (à droite ou à gauche), et là, il faut faire commit une fois que c'est fini.
- sur cette branche main modifiée, `git merge refac_no_comma`: avec un outil graphique de fusion, vous pouvez faire une
partie de la fusion automatiquement, et vous devez editer la partie centrale si vous voulez vraiment conserver les 
changements des deux versions.
